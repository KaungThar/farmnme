@extends('layouts.app')

@section('content')
	<div class="container-fluid">
		<div class="row">
			@forelse( $items as $item )
				<div class="col-md-3 custom-margin">
					<div class="card" style="width: 18rem;">
					  	<img class="card-img-top" src="{{ $item->image }}">
						 <div class="card-body">
						    <p class="card-text"> {{ $item->description }} </p>
						    <span class="tag">{{ $item->price }} MMK</span>
						    <a href="{{ route('add-to-cart', ['id' => $item->id]) }}" class="btn btn-primary">Put to Basket</a>
		  				</div>	
					</div>
				</div>	
			@empty
				<p> We have ran out of stock. It's like we have so much demand. </p>
			@endforelse
		</div>	
	</div>
@endsection