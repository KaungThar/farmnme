@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-3">
                @if(Session::has('message'))
                    <p class="alert {{ Session::get('alert-class', 'alert-info') }}">
                    {{ Session::get('message') }}</p>
                @endif
            </div>
            <div class="col-md-3">
                
                <video width="300" height="200" loop autoplay controls>
                <source src="https://res.cloudinary.com/dsosfuqap/video/upload/v1532557735/Farm2U/Tomatoes_-_14131.mp4">
                </video>

            </div>
            <div class="col-md-6 custom-margin-top">
                <p> A social enterprise that connects farms directly to you.</p>
                <br>
                <p>We pay farmers more.</p>
                <p>And we care you. Genuinely fresh veggies. </p>
                <p>Delivery to your door with sweet reasonable price. </p>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-md-4"></div>
            <div class="col-md-4">
                <p>Our service is now available in Yangon urban area. </p> 
                <p>We serve your command within 48 hours. </p>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-md-4"></div>
            <div class="col-md-4 text-center">
                <a href="https://www.facebook.com/farmnme/">
                    <img src="https://res.cloudinary.com/dsosfuqap/image/upload/v1532705476/Farm2U/icons8-facebook-50.png">
                </a>
                <a href="">
                    <img src="https://res.cloudinary.com/dsosfuqap/image/upload/v1532705484/Farm2U/icons8-email-50.png">
                </a>
                <a href="tel:+959770563818">
                    <img src="https://res.cloudinary.com/dsosfuqap/image/upload/v1532705464/Farm2U/icons8-phone-50.png">
                </a>
            </div>
        </div>
    </div>
<br><br>
    <div class="row ">
        <div class="col-md-8"></div>
        <div class="col-md-3">
            <p class="custom-font float-right"> Farm&me <img src="https://res.cloudinary.com/dsosfuqap/image/upload/v1532707614/icons8-copyright-16.png"> 2018. All rights reserved.
        </div>
    </div>
@endsection