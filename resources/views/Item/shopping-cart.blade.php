@extends('layouts.app')

@section('content')
	<div class="container-fluid">
		@if( Session::has('cart'))
			<div class="row">
				@foreach( $cart->items as $item)
					<div class="col-md-5 custom-margin-div">
						<li class="list-group-item">
							<strong> {{ $item['item']['name'] }} </strong>
							<span class="badge float-right custom-background"> {{ $item['qty'] }} </span> 
							<span class="badge badge-success"> {{ $item['price'] }}</span>
							<button class="btn btn-primary btn-xs dropdown-toggle" data-toggle="dropdown">
								Action <span class="caret"></span>
							</button>
							<ul class="dropdown-menu">
								<li><a href="{{ route('reduce-one', ['id' => $item['item']['id'] ]) }}">Reduce by 1</a></li>
								<li><a href="{{ route('remove-item', ['id' => $item['item']['id'] ]) }}">Reduce All</a></li>
							</ul>
						</li>	
					</div>
				@endforeach
			</div>
			<div class="row">
				<div class="col-md-5 custom-margin-div">
					@if($cart->totalPrice > 0)
					<strong> Total: {{ $cart->totalPrice > 0 ? $cart->totalPrice + 1000 + ($cart->totalPrice*0.05) : 0 }} MMK <p class="small">*Delivery and tax included</p> </strong>
					@endif
				</div>
			</div>
			<hr>
			<div class="row">
				<div class="col-md-5 custom-margin-div text-center">
					
					<!-- Button trigger modal -->
					<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalCenter">
					  Check Out
					</button>

					<!-- Modal -->
					<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
					  <div class="modal-dialog modal-dialog-centered" role="document">
					    <div class="modal-content">
					      <div class="modal-header">
					        <h5 class="modal-title" id="exampleModalLongTitle">Let's meet at your door within 48 hrs</h5>
					        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
					          <span aria-hidden="true">&times;</span>
					        </button>
					      </div>
					      <div class="modal-body">
					
					        {!! Form::open(['route' => 'store-order', 'id' => 'order_form']) !!}
					        	{{ Form::token() }}
					        	<div class="row">
					        		<div class="col-md-4">
					        			{{ Form::label('username', 'Name') }}
					        		</div>
					        		<div class="col-md-6">
					        			{{ Form::text('username', null, ['class' => 'form-control', ]) }}
					        		</div>
					        	</div>
					        	<br>

					        	<div class="row">
					        		<div class="col-md-4">
					        			{{ Form::label('place', 'Place') }}
					        		</div>
					        		<div class="col-md-6">
					        			{{ Form::text('place', null, ['class' => 'form-control'])}}
					        		</div>
					        	</div>
					        	<br>

					        	<div class="row">
					        		<div class="col-md-4">
					        			{{ Form::label('phone', 'Contact') }}
					        		</div>
					        		<div class="col-md-6">
					        			{{ Form::text('phone', null, ['class' => 'form-control']) }}
					        		</div>
					        	</div>	
					        	<br>		
								<input type="hidden" name="cart" value="{{ serialize($cart) }}">
					      </div>
					      <div class="modal-footer">
					        <button type="button" class="btn btn-secondary" data-dismiss="modal">Umm ?</button>
					        <button type="button" class="btn btn-success" data-dismiss="modal" onclick="form_submit()">Get to my door </button>
					        
					       	 {!! Form::close() !!}
					      </div>
					    </div>
					  </div>
			</div>
				</div>
			</div>
		@else
			<p>Fill up your empty basket?</p>	
		@endif
	</div>

	<script type="text/javascript">
  	function form_submit() {
    document.getElementById("order_form").submit();
   	}    
  </script>
@endsection