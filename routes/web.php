<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/sample', 'ItemController@sample')->name('get-sample');
Route::get('/', 'ItemController@index')->name('buy-from-farm');
Route::get('/add-to-cart/{id}', 'ItemController@addToCart')->name('add-to-cart');
Route::get('/shopping-cart', 'ItemController@getShoppingCart')->name('shopping-cart');
Route::get('/reduce/{id}', 'ItemController@getReduceByOne')->name('reduce-one');
Route::get('/remove/{id}', 'ItemController@getRemoveItem')->name('remove-item');
Route::post('/store-order', 'OrderController@store')->name('store-order');
Route::get('/home', function () {
    return view('welcome');
})->name('home');
