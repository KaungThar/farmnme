<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Farm & Me') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,600" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/custom.css') }}" rel="stylesheet">

</head>
<body>
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-light navbar-laravel">
            <div class="container">
               
                    <img src="https://res.cloudinary.com/dsosfuqap/image/upload/v1532684187/Image.jpg" class="navbar-brand custom-logo">
       
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">
                        
                        <li class="nav-item nav-item-padding">
                            <a class="custom-link" href="{{ url('/') }}"> {{ __('Buy from Farm') }}</a>
                        </li>
                        <li class="nav-item nav-item-padding">
                            <a class="custom-link" href="{{ route('shopping-cart') }}"> {{ __('My Basket') }}</a>
                            <span class="badge custom-background"> {{ Session::has('cart') ? Session::get('cart')->totalQty : '' }} </span>
                        </li>
                        <li class="nav-item nav-item-padding">
                            <a class="custom-link" href="{{ route('home') }}"> {{ __('Who we are') }}</a>
                        </li>


                    </ul>

                   
                    
                </div>
            </div>
        </nav>

        <main class="py-4">
            @yield('content')
        </main>
    </div>
</body>
    <script
  src="https://code.jquery.com/jquery-3.3.1.min.js"
  integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
  crossorigin="anonymous"></script>
  <script type="text/javascript">
      $("document").ready(function(){
    setTimeout(function(){
        $("p.alert").remove();
    }, 3000 ); // 5 secs
});

  </script>
</html>
