@extends('layouts.app')

@section('content')
	<div class="container-fluid">
		<div class="row custom-botton-margin">
			
			<div class="col-md-3 custom-margin">
				<div class="card" style="width: 18rem;">
				  	<img class="card-img-top" src="{{ asset('images/farm&me.png') }}" alt="Card image cap">
					 <div class="card-body">
					    <h5 class="card-title">Card title</h5>
					    <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
					    <a href="#" class="btn btn-primary">Go somewhere</a>
	  				</div>
				</div>
			</div>

			<div class="col-md-3 custom-margin">
				<div class="card" style="width: 18rem;">
				  	<img class="card-img-top" src="{{ asset('images/farm&me.png') }}" alt="Card image cap">
					 <div class="card-body">
					    <h5 class="card-title">Card title</h5>
					    <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
					    <a href="#" class="btn btn-primary">Go somewhere</a>
	  				</div>
				</div>
			</div>
		
			<div class="col-md-3 custom-margin">
				<div class="card" style="width: 18rem;">
				  	<img class="card-img-top" src="{{ asset('images/farm&me.png') }}" alt="Card image cap">
					 <div class="card-body">
					    <h5 class="card-title">Card title</h5>
					    <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
					    <a href="#" class="btn btn-primary">Go somewhere</a>
	  				</div>
				</div>
			</div>

		</div>



		<div class="row">
			
			<div class="col-md-3 custom-margin">
				<div class="card" style="width: 18rem;">
				  	<img class="card-img-top" src="{{ asset('images/farm&me.png') }}" alt="Card image cap">
					 <div class="card-body">
					    <h5 class="card-title">Card title</h5>
					    <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
					    <a href="#" class="btn btn-primary">Go somewhere</a>
	  				</div>
				</div>
			</div>

			<div class="col-md-3 custom-margin">
				<div class="card" style="width: 18rem;">
				  	<img class="card-img-top" src="{{ asset('images/farm&me.png') }}" alt="Card image cap">
					 <div class="card-body">
					    <h5 class="card-title">Card title</h5>
					    <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
					    <a href="#" class="btn btn-primary">Go somewhere</a>
	  				</div>
				</div>
			</div>
		
			<div class="col-md-3 custom-margin">
				<div class="card" style="width: 18rem;">
				  	<img class="card-img-top" src="{{ asset('images/farm&me.png') }}" alt="Card image cap">
					 <div class="card-body">
					    <h5 class="card-title">Card title</h5>
					    <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
					    <a href="#" class="btn btn-primary">Go somewhere</a>
	  				</div>
				</div>
			</div>
			
		</div>
	</div>
@endsection